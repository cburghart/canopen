---
title: "New release: v2.1.2 and v2.0.4"
author: jseldent
categories: release
---

v2.1.2 and v2.0.4 have just been released. These are minor releases containing
only bugfixes.

### Bug fixes

The following bug has been fixed in both v2.1.2 and v2.0.4:

- The SYNC start and interval calculation now also works for SYNC perios larger
  than one second.

The following bugs have been fixed in v2.1.2:
- A diagnostic message is printed in case of file errors in the DCF parser and
  the SDO UploadFile/DownloadFile callbacks.
- Use `generic_category()` instead of `system_category()` when creating
  `std::error_code` instances. This follows the convention used by
  `std::make_error_code()` and fixes comparisons with `std::errc` values.
- Add a 100 ms timeout to CAN frame write confirmations in `IoContext`. This
  prevents CAN frame transmission from permanently blocking once a frame cannot
  be put on the bus (because of bus off or a broken cable). If a timeout occurs,
  the contents of the user-space transmit queue in `IoContext` are discarded to
  prevent old CAN frames from appearing on the bus.
- `IoContext` now prints diagnostic messages in case of CAN bus errors or state
  changes. In case of repeating errors, such as multiple write confirmation
  timeouts, only the first error is reported. Once the error is resolved, the
  total number of errors is reported as an informational diagnostic.

### Download

You can download the source from
[GitLab](https://gitlab.com/lely_industries/lely-core/-/archive/v2.1.2/lely-core-v2.1.2.zip)
or the Ubuntu packages from our
[PPA](https://launchpad.net/~lely/+archive/ubuntu/ppa).
