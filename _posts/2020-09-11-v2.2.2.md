---
title: "New release: v2.2.2, v2.1.5 and v2.0.6"
author: jseldent
categories: release
---

v2.2.2, v2.1.5 and v2.0.6 have just been released. These are minor releases
containing only bugfixes.

### Bug fixes

The following bugs have been fixed in all three versions:
- `rbtree_insert()` and `rbtree_remove()` would sometimes omit updating the node
  counter in a red-black tree.
- Transmit-PDOs would miss RTR frames because the RTR flag was not set in the
  receiver.
- Transmit-PDOs would omit the IDE flag when using extended indentifiers.
- The user would not be notified of an NMT hearbeat timeout resulution.
- New build warnings issued by GCC 10.1 have been fixed.

The following additional bugs have been fixed in v2.2.2:
- CAN channel I/O event handling is now more robust on Linux.
- The `USleep()` method in `LoopDriver` no longer throws an exception when the
  suspension time has elapsed.
- Destroying an I/O service while it has pending task can lead to a busy loop in
  the destructor. A warning is now printed when this situation is detected.
- The DCF tools are now more lenient when parsing the `PDOMapping` and
  `ObjectType` attributes.
- `dcfgen` now sets the `ObjectType` of the RPDO and TPDO mapping objects to
  RECORD instead of ARRAY, as specified by CiA 301.
- The `--no-strict` option has been added `dcfgen`, which allows the user to
  skip the DCF linter for slave EDS/DCF files.

### Download

You can download the source from
[GitLab](https://gitlab.com/lely_industries/lely-core/-/archive/v2.2.2/lely-core-v2.2.2.zip)
or the Ubuntu packages from our
[PPA](https://launchpad.net/~lely/+archive/ubuntu/ppa).
