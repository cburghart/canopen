---
title: "Lely CANopen in space"
author: jseldent
categories: news
---

Last year, the European Space Agency (ESA) issued an invitation to tender for
the development of an open-source implementation of the CANopen protocol
suitable for space applications. In particular, an implementation of the
[ECSS-E-ST-50-15C - CANbus extension protocol](https://ecss.nl/standard/ecss-e-st-50-15c-space-engineering-canbus-extension-protocol-1-may-2015/)
with accompanying test suite. The Lely CANopen stack was one of the candidates
considered to be a promising starting point for such a development.
[N7 Space](http://n7space.com/) submitted a proposal based on our stack and won
the bid. Since the beginning of this year, they have been working, with support
from [Lely](https://www.lely.com/), to improve and extend the stack and to
develop a comprehensive test suite. This work is carried out under a programme
of, and funded by, the European Space Agency.

Several of their improvements have already found their way back into recent
releases. And even more will be part of the upcoming v2.3 release. However, some
changes require us to break the backward compatibility of part of the C API. For
example, one of the ESA requirements is that the stack can be used with (almost)
no support from the standard C library. Supporting such a _freestanding_
environment, with no dynamic memory allocation, affects several public
interfaces.

Breaking changes warrant an increase in the major version number. For now, v3 is
under development in the `ecss` branch at
<https://gitlab.com/n7space/canopen/lely-core>. The first releases mark
milestones in the ESA project. Once it is finished, the `ecss` branch will be
merged into the
[official repository](https://gitlab.com/lely_industries/lely-core) and released
publicly, probably with version 3.3. Non-breaking changes will continue to be
merged into v2.x releases.

**Disclaimer:** The views expressed here can in no way be taken to reflect the
official opinion of the European Space Agency.
{: .notice--info}
