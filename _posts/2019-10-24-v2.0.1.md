---
title: "New release: v2.0.1"
author: jseldent
categories: release
---

v2.0.1 has just been released. This is a minor release containing only bugfixes
and cleanups. No new functionality has been added and no behavior has changed.

**Note:** The `__likely()` and `__unlikely()` macros, which were deprecated in
the previous release, have now been removed.
{: .notice--warning}

### Static code analysis

Most of the bugs fixed in this release were discovered by two static code
analysis tools we've started using since the previous release:
- [Cppcheck](http://cppcheck.sourceforge.net/), which is run on every commit;
  any defect it reports is considered a build failure.
- [Coverity Scan](https://scan.coverity.com/), which is run manually on the
  master branch. The results of the analysis can be found at
  <https://scan.coverity.com/projects/lely-core-libraries>.

As of this writing, Coverity Scan reports 4 outstanding defects. These are false
positives which, for some reason, we cannot get Coverity to ignore.
{: .notice--info}

These tools are used in addition to the
[Clang Static Analyzer](https://clang-analyzer.llvm.org/) and
[cpplint](https://github.com/cpplint/cpplint), which have been in use for a
while now.

### Download

You can download the source from
[GitLab](https://gitlab.com/lely_industries/lely-core/-/archive/v2.0.1/lely-core-v2.0.1.zip)
or the Ubuntu packages from our
[PPA](https://launchpad.net/~lely/+archive/ubuntu/ppa).
