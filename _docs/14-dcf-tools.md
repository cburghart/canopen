---
title: "EDS/DCF tools"
permalink: /docs/dcf-tools/
toc: true
---

The `dcf-tools` Python module contains tools to parse and generate EDS/DCF
files. Most importantly, it contains the `dcfgen` tool, which can be used to
generate the DCF of a CANopen master.

## dcfchk

The `dcfchk` tool can be used to check an EDS/DCF file for errors. It takes a
single argument: the name of the file to be checked. Any issues are printed as
warnings to `stderr`. The tool terminates with exit code `1` if issues are
found, and `0` if not.

### Options

Option                  | Description
----------------------- | -----------
`-n ID`, `--node_id ID` | The node-ID (default: `255`).
`-p`, `--print`         | Print the PDO mappings.

## dcfgen

The `dcfgen` tool has a single argument: the name of the YAML file containing
the configuration of the master. It generates the file `master.dcf` and,
optionally, `master.bin`, containing the serial number of the master and the
revision and serial numbers of the slaves, as well as their expected software
version. For each slave, a `.bin` file is generated which contains the SDO
requests used to configure the slave during boot-up.

### Options

Option                      | Description
--------------------------- | -----------
`-d DIR`, `--directory DIR` | The directory in which to store the generated file(s).
`-r`, `--remote-pdo`        | Generate remote PDO mappings.
`-S`, `--no-strict`         | Do not abort in case of an invalid slave EDS/DCF.
`-v`, `--verbose`           | Print generated the SDO requests.

### YAML configuration

The YAML file has two special sections: `options` and `master`. All other
sections are assumed to describe slaves, unless their name begins with `.`, in
which case they are ignored.

#### `options`

The `options` section is optional and contains default values for some of the
parameters in the master and slave sections:

Keyword                | Description
---------------------- | -----------
`dcf_path`             | The directory in which the generated `.dcf` and `.bin` files will be available at runtime (default: `""`).
`heartbeat_multiplier` | The multiplication factor used to obtain heartbeat consumer times from heartbeat producer times (default: `3.0`).

#### `master`

The `master` section is mandatory and has the following parameters:

Keyword                | Description
---------------------- | -----------
`node_id`              | The node-ID (default: `255`).
`baudrate`             | The baudrate in kbit/s (default: `1000`).
`vendor_id`            | The vendor-ID (default: `0x00000000`).
`product_code`         | The product code (default: `0x00000000`).
`revision_number`      | The revision number (default: `0x00000000`).
`serial_number`        | The serial number (default: `0x00000000`).
`heartbeat_multiplier` | The multiplication factor used to obtain the slave heartbeat consumer time from the master heartbeat producer time (default: see `options` section).
`heartbeat_consumer`   | Specifies whether the master should monitor the heartbeats of the slaves (default: `true`).
`heartbeat_producer`   | The heartbeat producer time in ms (default: `0`).
`emcy_inhibit_time`    | The EMCY inhibit time in multiples of 100 μs (default: `0`, see object 1015).
`sync_period`          | The SYNC interval in μs (default: `0`).
`sync_window`          | The SYNC window length in μs (default: `0`, see object 1007).
`sync_overflow`        | The SYNC counter overflow value (default: `0`, see object 1019).
`error_behavior`       | A dictionary of error behaviors for different classes or errors (default: `{1: 0x00}`, see object 1029).
`nmt_inhibit_time`     | The NMT inhibit time in multiples of 100 μs (default: `0`, see object 102A).
`start`                | Specifies whether the master shall switch into the NMT operational state by itself (default: `true`, see bit 2 in object 1F80).
`start_nodes`          | Specifies whether the master shall start the slaves (default: `true`, see bit 3 in object 1F80).
`start_all_nodes`      | Specifies whether the master shall start all nodes simultaneously (default: `false`, see bit 1 in object 1F80).
`reset_all_nodes`      | Specifies whether all slaves shall be reset in case of an error event on a mandatory slave (default: `false`, see bit 4 in object 1F80).
`stop_all_nodes`       | Specifies whether all slaves shall be stopped in case of an error event on a mandatory slave (default: `false`, see bit 6 in object 1F80).
`boot_time`            | The timeout for booting mandatory slaves in ms (default: `0`, see object 1F89).

#### Slaves

All other sections are assumed to describe slaves. The name of the section is
the basename for the generated `.bin` file, if any. The slave section has the
following parameters:

Keyword                 | Description
----------------------- | -----------
`dcf`                   | The filename of the EDS/DCF describing the slave (mandatory).
`dcf_path`              | The directory in which the generated `.bin` file will be available at runtime (default: see `options` section).
`node_id`               | The node-ID (default: `255`, can be omitted if specified in the DCF).
`revision_number`       | The revision number (default: `0x00000000`, can be omitted if specified in the DCF).
`serial_number`         | The serial number (default: `0x00000000`, can be omitted if specified in the DCF).
`heartbeat_multiplier`  | The multiplication factor used to obtain master heartbeat consumer time from the slave heartbeat producer time (default: see `options` section).
`heartbeat_consumer`    | Specifies whether the slave should monitor the heartbeat of the master (default: `false`).
`heartbeat_producer`    | The heartbeat producer time in ms (default: `0`).
`error_behavior`        | A dictionary of error behaviors for different classes or errors (default: `{}`, see object 1029).
`rpdo`                  | The Receive-PDO configuration (see below).
`tpdo`                  | The Transmit-PDO configuration (see below).
`boot`                  | Specifies whether the slave will be configured and booted by the master (default: `true`, see bit 2 in object 1F81).
`mandatory`             | Specifies whether the slave is mandatory (default: `false`, see bit 3 in object 1F81).
`reset_communication`   | Specifies whether the NMT reset communication command may be sent to the slave (default: `true`, see bit 4 in object 1F81).
`software_file`         | The name of the file containing the firmware (default: `""`, see object 1F58).
`software_version`      | The expected software version (default: `0x00000000`, see object 1F55).
`configuration_file`    | The name of the file containing the configuration (default: `"<dcf_path>/<name>.bin"` (where `<name>` is the section name), see object 1F22).
`restore_configuration` | The sub-index of object 1011 to be used when restoring the configuration (default: `0x00`).
`sdo`                   | Additional SDO requests to be sent during configuration (see below).

##### `rpdo` and `tpdo`

The `rpdo` and `tpdo` entries in a slave section can be used to configure
communication and mapping parameters of pre-existing Receive-PDOs and
Transmit-PDOs on a slave. Each entry in `rpdo` and `tpdo` is a number (starting
from `1`) containing one or more of the following options:

Keyword          | Description
---------------- | -----------
`enabled`        | Specifies whether the PDO is enabled (default: `true`, if the COB-ID is valid).
`cob_id`         | The COB-ID. If the most significant bit is set, the PDO will be ignored by the master. If the value is `auto`, an unused 11-bit COB-ID will be assigned. This can be used for at most 96 PDOs outside the pre-defined connection set (COB-ID: `0x680` - `0x6DF`).
`transmission`   | The transmission type.
`inhibit_time`   | The inhibit time of the (corresponding master) TPDO in multiples of 100 μs.
`event_timer`    | The event timer of the (corresponding master) TPDO in ms.
`event_deadline` | The deadline for (corresponding master) RPDO reception in ms.
`sync_start`     | The SYNC start value of the (corresponding master) TPDO.
`mapping`        | The PDO mapping. This is an array of dictionaries, each containing an `index` and `sub_index` entry.

##### `sdo`

For each slave, additional SDO write requests can be specified. They are
executed after the automatically generated SDO requests for configuring
heartbeats and PDOs.

The `sdo` entry is an array of dictionaries, each containing the following
entries:

Keyword     | Description
----------- | -----------
`index`     | The object index (mandatory).
`sub_index` | The object sub-index (default: `0x00`).
`value`     | The value to be written (default: `0`).
